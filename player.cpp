#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    this->side = side;
     
}

/*
 * Destructor for the player
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	Side opp = (side == BLACK) ? WHITE : BLACK;
    board.doMove(opponentsMove, opp);

    vector<Move*> moves = getValidMoves(this->side, this->board);

    if(moves.empty()) {
    	return NULL;
    }
    
    Move *move = NULL;
    
    if(testingMinimax) {
        move = minimaxNaive(moves);
    }
    else {
        move = minimax(moves);//move = heuristic(moves);//
    }
    
    board.doMove(move, side);

    return move;
}

/*
 * @brief Writes a lists with all the possible moves in the board
 */
vector<Move*> Player::getValidMoves(Side side, Board board) {
	vector<Move*> moves;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			Move *tmp = new Move(i, j);
			if(board.checkMove(tmp, side)) {
				moves.push_back(tmp);
			}
		}
	}
	return moves;
}

int Player::MoveValue(Move *move, Board board, Side side){
    int value = MoveValueNaive(move, board, side);
    int x = move->getX();
    int y = move->getY();
    if(x == 0 || x == 7){
        value *= 18;
        if(y == 1 || y == 6){
            value *= -1;
        }
    }
    if(y == 0 || y == 7){
        value *= 18;
        if(x == 1 || x == 6){
            value *= -1;
        }
    }
    if((y==1 && x==1) || (y==6 && x==1) || (y==1 && x==6) || (y==6 && x==6)){
        value *= -18;
    }

    return value;
}

int Player::MoveValueNaive(Move *move, Board board, Side side) {
    Side opp = (side == BLACK) ? WHITE : BLACK;
    Board temp = *(board.copy());
    temp.doMove(move, side);
    int value = (temp.count(side) - temp.count(opp)) - (board.count(side) - board.count(opp));
    return value;
}

Move *Player::heuristic(vector<Move*> moves) {
    int max = -1152, temp;
    Move* MoveMax;
    for(unsigned int i = 0; i < moves.size(); i++){
        temp = MoveValue(moves[i], this->board, this->side);
        if(temp > max){
            max = temp;
            MoveMax = moves[i];
        }
    }
    return MoveMax;
} 



Move *Player::minimaxNaive(vector<Move*> moves) {
    int max = -123456789, temp;
    Move* MoveMax;
    for(unsigned int i = 0; i < moves.size(); i++){
        temp = MoveValueNaive(moves[i], this->board, this->side) - opponentMaxScoreNaive(moves[i]);
        if(temp > max){
            max = temp;
            MoveMax = moves[i];
        }
    }
    return MoveMax;
}

Move *Player::minimax(vector<Move*> moves) {
    int max = -123456789, temp;
    Move* MoveMax;
    for(unsigned int i = 0; i < moves.size(); i++){
        temp = MoveValue(moves[i], this->board, this->side) - opponentMaxScore(moves[i]);
        if(temp > max){
            max = temp;
            MoveMax = moves[i];
        }
    }
    return MoveMax;
}


/*
 * @input board Board without move
 *
 * @return Highest possible score that opponent can achieve
 */
int Player::opponentMaxScoreNaive(Move *move) {
    Board board = *(this->board.copy());

    Side opp = (side == BLACK) ? WHITE : BLACK;

    board.doMove(move, this->side);

    vector<Move*> moves = getValidMoves(opp, board);

    if(moves.empty()) {
        return 0;
    }
    
    int max = -12345678, temp;
    for(unsigned int i = 0; i < moves.size(); i++){
        temp = MoveValueNaive(moves[i], board, opp);
        if(temp > max){
            max = temp;
        }
        fprintf(stderr, "Our move: %d, %d; Opp move: %d, %d\n", move->getX(), move->getY(), moves[i]->getX(), moves[i]->getY());
        fprintf(stderr, "Our score: %d; Opp score: %d\n", MoveValueNaive(move, this->board, this->side), temp);
    }

    return max;
}

int Player::opponentMaxScore(Move *move) {
    Board board = *(this->board.copy());

    Side opp = (side == BLACK) ? WHITE : BLACK;

    board.doMove(move, this->side);

    vector<Move*> moves = getValidMoves(opp, board);

    if(moves.empty()) {
        return 0;
    }
    
    int max = -12345678, temp;
    for(unsigned int i = 0; i < moves.size(); i++){
        temp = MoveValue(moves[i], board, opp);
        if(temp > max){
            max = temp;
        }
        //fprintf(stderr, "Our move: %d, %d; Opp move: %d, %d\n", move->getX(), move->getY(), moves[i]->getX(), moves[i]->getY());
        //fprintf(stderr, "Our score: %d; Opp score: %d\n", MoveValue(move, this->board, this->side), temp);
    }

    return max;
}
