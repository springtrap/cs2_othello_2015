Fabio: wrote most of the heuristic algorithm, helped with the othello strategies, debugging

Dennis: wrote most of the minimax algorithm, helped with the coding style, debugging

Both of us worked together in the same room for the whole assigment so we could easily communicate what we
wanted for each part of the assigment and we could help each other degug.

Improvements:

In our heuristic function, we prioritized corners (assigned a multiplier to the score), as they are guaranteed for
 the rest of the game. We also prioritized edges. However, the points surrounding the corners were de-prioritized 
 so that it would nearly be impossible for them to be chosen.

In addition, our minimax function implements the naive and updated heuristic functions for scoring moves. 
This way, the scores calculated for the opponent changed.

We found that improving the heuristic function allowed us to beat the ConstantTime and Basic players. 
Also, the minimax function allowed us to beat both of them.

We would want to improve the depth of the minimax function, or add more statistics to the heuristic function
 in order to get a more accurate score.