#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include <cstdio>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board board;
    Side side;
private:
	vector<Move*> getValidMoves(Side side, Board board);
	int MoveValue(Move *move, Board board, Side side);
    int MoveValueNaive(Move *move, Board board, Side side);
	Move *heuristic(vector<Move*> moves);
    int opponentMaxScoreNaive(Move *move);
    Move *minimaxNaive(vector<Move*> moves);
    int opponentMaxScore(Move *move);
    Move *minimax(vector<Move*> moves);
};

#endif
